# WebApp Theme Builder #
------------------------

Index
-----

* [Overview](#overview)
* [Requirements](#requirements)
* [Getting started](#getting-started)
* [Creating/updating themes](#creatingupdating-themes)
* [Deploying themes](#deploying-themes)
* [Upgrading the WebApp Theme Builder tool](#upgrading-the-webapp-theme-builder-tool)
* [Additional ``buildthemes`` command options](#additional-buildthemes-command-options)
* [Changelog](#changelog)


## Overview ##

The Covata/WebApp Theme Builder tool allows you to build themes (locally on your own computer) for SafeShare's web-based interfaces.

Themes allow the color schemes and logos utilised throughout the [SafeShare web application](https://docs.covata.com/latest/user/index.html) user interface to be customised for individual [organisations](https://docs.covata.com/latest/user/index.html#organisations), where each individual organisation is associated with its own theme. (For more information about adding/configuring organisations through [SafeShare Administration](https://docs.covata.com/latest/admin-platform/index.html#platform-administration), see [Administering organisations](https://docs.covata.com/latest/admin-platform/administering-organisations.html) in the *SafeShare Administrator's Guide*.)

These color scheme and logo customisations are also utilised throughout:

* the [Organisation Administration](https://docs.covata.com/latest/admin-org/index.html#organisation-administration) interface,
* the [SafeShare Administration](https://docs.covata.com/latest/admin-platform/index.html#platform-administration) interface, as well as
* email notifications sent out by the Covata Platform.

Newly installed instances of SafeShare are pre-packaged with the *Covata default theme*. This theme is initially used for all organisations' SafeShare web application and Organisation Administration interfaces, email notifications, as well as the SafeShare Administration interface.

Using this WebApp Theme Builder tool, you can generate different themes - one for each organisation - as well as your own default theme (aka the 'system' theme), which is used for:

* any organisation that does not have a theme defined for it,
* the SafeShare Administration interface and
* the **My Account** feature.

Be aware that:

* This default/'system' theme is distinct from the *Covata default theme* (pre-packaged with SafeShare).
* When using the WebApp Theme Builder tool, you must generate the 'system' theme.


## Requirements ##

The WebApp Theme Builder tool requires the following to be installed on your local machine:

* [Node.js](https://nodejs.org/en/)

For each organisation you wish to create a theme for, make a note of its Organisation ID. A SafeShare administrator can obtain an Organisation's ID through the **Organisations** page of SafeShare Administration. (For more information, see [Administering organisations](https://docs.covata.com/latest/admin-platform/administering-organisations.html) in the *SafeShare Administrator's Guide*.)


## Getting started ##

> **Tip:** You should only need to run through this **Getting Started** procedure once. If you are updating existing and/or creating more themes, please skip to the [Creating/updating themes](#creatingupdating-themes) section below.

10. Ensure you have met the [Requirements](#requirements) (above).

20. Open your terminal/command prompt.

30. Run the command ``sudo npm install covata-theme-builder -g`` to install the WebApp Theme Builder tool.

40. Do either of the following to set up your local 'working' ``sources`` directory (which the WebApp Theme Builder tool will use to generate themes):

    * Manually set up your ``sources`` directory:

        10. Create a directory named ``sources`` at an appropriate location on your computer.

        20. Within this ``sources`` directory, create subdirectory named ``system``.

        30. Visit Covata's public BitBucket repository for the [source of the WebApp Theme Builder](https://bitbucket.org/covata/webapp-theme-builder/src) tool and click through to the ``sources/system`` folder.

        40. At this location:

            10. Click the ``theme.properties`` file to access its content.

            20. Copy and paste this content into a text editor.

            30. Save this text file with the same name (``theme.properties``) in the ``system`` subdirectory created above.

        50. Navigate back to the ``sources/system`` folder and note the image/logo files. These types of files are required for this tool to successfully build a theme.

    * Set up your ``sources`` directory from the example provided within the WebApp Theme Builder tool's installation directory:

        10. Identify the directory where the WebApp Theme Builder tool was installed by the ``npm`` command. This directory should be named ``covata-theme-builder``.

            > **Note:** On Mac OS X / Linux-based computers, this directory is typically within ``/usr/local/lib/node_modules``. On Microsoft Windows-based computers, this directory is the location of the ``node.exe`` binary.

        20. Copy the ``sources`` directory (within the ``covata-theme-builder`` installation directory) to an appropriate location on your computer.

> **Note:** The directory named ``system`` (within ``sources``) contains the theme resources - i.e. color schemes and logos - for the default/'system' theme.


## Creating/updating themes ##

Ensure you have completed the [Getting Started](#getting-started) procedure above before proceeding.

> **Notes:**
> *    If you are only updating existing themes, begin at step 3, although skip steps 3 and 4 for any existing themes you do not intend to change.
> *    Check if you are running the latest version of the WebApp Theme Builder tool using the command ``buildthemes --version``. If you are not, follow the [Upgrading the WebApp Theme Builder tool](#upgrading-the-webapp-theme-builder-tool) procedure below before continuing.

10. Create new directories (one for each new organisation a theme will be created for) immediately inside the ``sources`` directory you set up (above).

    > **Important:** The names of these directories must match their respective Organisation IDs (obtained in [Requirements](#requirements) above) and each of these directories must be siblings of the ``system`` directory.

20. For each (sub-)directory you created in the previous step, copy the ``theme.properties`` file from the ``sources/system`` subdirectory to each of these other ``sources/{OrganisationID}`` subdirectories.

30. For each theme, copy their image/logo files to their respective ``sources/{OrganisationID}`` subdirectories (similar to the ``sources/system`` subdirectory for the default/'system' theme):

    * ``header-logo.(png|jpg|gif|bmp)`` - The image/logo used in the header of the SafeShare web application and Organisation Administration interfaces.

        > **Notes:**
        > *    The image/logo in the ``system`` directory is used for the default organisation theme, as well as for the SafeShare Administration interface. This principle applies to the other logo files described below.
        > *    For simplicity (espcially when [upgrading the WebApp Theme Builder tool](#upgrading-the-webapp-theme-builder-tool)), ensure your image/logo file names match the names mentioned in this list.

    * ``footer-logo.(png|jpg|gif|bmp)`` - The image/logo used in the footer of the SafeShare web application on the main Sign-in page.
    
    * ``footer-logo-white.(png|jpg|gif|bmp)`` - The image/logo used in the footer of the SafeShare web application and Organisation Administration interfaces after user sign-in.

    * ``favicon.ico`` - A 16x16 pixel (organisation-specific) favicon used by web browsers (e.g. on browser tabs).

    * ``email-logo.(png|jpg|gif|bmp)`` - The image/logo used for *all* email notifications sent out by the Covata Platform.

        > **Note:** SafeShare only makes use of the image/logo contained in the ``system`` directory. Therefore, copy this image/logo file contained in the ``system`` directory to all other organisation directories. (If an ``email-logo`` image file is not present in any organisation directory, then running the ``buildthemes`` command towards the end of this procedure will fail.)

    * ``login-logo.(png|jpg|gif|bmp)`` - The image/logo used on the Covata/SafeShare Sign-in page.

        > **Note:** SafeShare only makes use of the image/logo contained in the ``system`` directory. Therefore, for the same reason that applies to the ``email-logo`` file (above), copy the ``login-logo`` image file contained in the ``system`` directory to all other organisation directories.

40. For each theme, configure the values of each property in its ``theme.properties`` file (copied to the theme's subdirectory within ``sources`` above), with the theme's colors and image file names:

    > **Tip:** Refer to the [Themable areas of the SafeShare web application](https://docs.covata.com/latest/admin-platform/theming-the-applications-web-based-interfaces.html#themable-areas-of-the-web-application) and [Other themable SafeShare areas](https://docs.covata.com/latest/admin-platform/theming-the-applications-web-based-interfaces.html#other-themable-application-areas) sections of the *SafeShare Administrator's Guide* for diagrams that depict where the following properties affect SafeShare's user interfaces.

    * ``header-logo`` - The name of the header image/logo file (located in this directory).

    * ``footer-logo`` - The name of the footer image/logo file as seen on the sign-in page (located in this directory).

    * ``footer-logo-white`` - The name of the footer image/logo file as seen after user sign-in (located in this directory).

    * ``email-logo`` - The name of the email image/logo file (located in this directory).

    * ``login-logo`` - The name of the sign-in page image/logo file (located in this directory).

    * ``favicon`` - The name of the favicon file (located in this directory).

    * ``header-color`` - The background color used for the header bar of the SafeShare web application and Organisation Administration interfaces, which also includes the header of email notifications.

        > **Note:** The value of this property defined in the ``system`` directory's ``theme.properties`` file is used for the default organisation theme, the SafeShare Administration interface and *all* email notifications. This principle applies to the other color properties described below.

    * ``header-text-color`` - The text color used in the header bar of the SafeShare web application and Organisation Administration interfaces. Be aware that this excludes the color of any text forming part of the ``header-logo`` image/logo (described above).

    * ``hyperlink-active-color`` - The text color used when the **Application menu** at the top-right of the SafeShare web application or Organisation Administration interface (which contains the signed-in user's email address) is selected.

    * ``primary-color`` - The main application color used for:

        * The **Folder** button in the SafeShare web application.

        * The background color of dialog box headers throughout the SafeShare web application and Organisation Administration interfaces.

        * The background color of the left selected option on the Organisation Administration interface.

        * The **SIGN IN** button on the sign-in page.

            > **Note:** SafeShare only makes use of the value of this property defined in the ``system`` directory's ``theme.properties`` file.

    * ``primary-button-color`` - The color used for buttons associated with (i.e. to complement) areas where the ``primary-color`` is expressed. This can be considered the secondary color of SafeShare's web-based interfaces, which includes:

        * The **Upload** button in the SafeShare web application.

        * Action buttons on:

            * Dialog boxes used throughout the SafeShare web application and Organisation Administration interfaces.

            * Pages throughout SafeShare's web-based **My Account** feature and on pages throughout the Organisation Administration interface.

    * ``create-button-color`` - The color used for the **Create** button in the SafeShare web application.

    * ``email-divider-color`` - The color used for the underlining band below the ``header-color`` block in email notifications, whose value is defined in the ``system`` directory's ``theme.properties`` file.

        > **Note:** SafeShare only makes use of the value of this property defined in the ``system`` directory's ``theme.properties`` file. Therefore, copy this property's value to the ``theme.properties`` file in all other organisations' directories. (If a valid value is not specified for this property in the ``theme.properties`` file of any organisation directory, then running the ``buildthemes`` command towards the end of this procedure will fail.)

    * ``selected-item-color`` - The color used for a selected item on the main SafeShare page.

    * ``selected-item-text-color`` - The text color used for a selected item on the main SafeShare page.

    * ``footer-color`` - The background color used for the footer bar of the SafeShare web application and Organisation Administration interfaces.

    * ``footer-text-color`` - The text color used in the footer bar of the SafeShare web application and Organisation Administration interfaces. Be aware that this excludes the color of any text forming part of the ``footer-logo`` image/logo (decsribed above).

    * ``alert-success-color`` - The background color used for the success alert popup of the SafeShare web application and Organisation Administration interfaces.

    * ``alert-success-text-color`` - The text color used in the  success alert popup of the SafeShare web application and Organisation Administration interfaces. Be aware that this excludes the color of any text forming part of the ``footer-logo`` image/logo (decsribed above).

50. If you do not intend to use any [Additional ``buildthemes`` command options](#additional-buildthemes-command-options) (see below for details), ensure your current directory is the parent directory of ``sources``.

60. Compile your themes by running the command ``buildthemes``.

    Assuming you have followed the procedure above correctly, the ``buildthemes`` command generates an ``output`` directory (based on the contents of the ``sources`` directory), whose content is deployed to the SafeShare web application installation directory.

    > **Tip:** The ``buildthemes`` command can be run without the need to specify a path.


## Deploying themes ##

Once you have [created/updated your themes](#creatingupdating-themes) (above), copy the ``output`` directory to the root directory of the SafeShare web application (of the Covata Platform instance) and rename this ``output`` directory to ``themes``.

Typically, this process would be conducted by a systems administrator who installed your SafeShare/Covata Platform instance, which is deployed using [Puppet](https://puppet.com/). Therefore, this theme deployment process would also be managed by Puppet.

Covata provides two BitBucket repositories for managing Puppet-based deployments of SafeShare instances:

* [SafeShare Puppet module](https://bitbucket.org/covata/puppet_covata_services) - contains Puppet configurations for the core components of the SafeShare instance.

* [Reference tree](https://bitbucket.org/covata/reference_tree_delivery) - contains additional Puppet configurations that support the SafeShare Puppet module above, as well as additional configurations for assisting with deployments of SafeShare to virtual machines (using Vagrant) and other hardware.

> **Note:** All themes compiled by running ``buildthemes`` must be deployed to a Covata Platform instance. Copying a single theme's subdirectory from your local ``output`` folder to the ``themes`` directory of the SafeShare web application will not work.


## Upgrading the WebApp Theme Builder tool ##

Ensure you have completed the [Getting Started](#getting-started) procedure above before proceeding.

10. Run the command ``sudo npm update covata-theme-builder -g`` to update the WebApp Theme Builder tool.

    > **Note:** Check to ensure that this command has upgraded the WebApp Theme Builder tool to the version indicated on this page by running the command ``buildthemes --version``. If this ``npm update`` did not successfully upgrade the tool, run the following commands:
    > 10.    ``sudo npm uninstall covata-theme-builder -g``
    > 20.    ``sudo npm install covata-theme-builder -g``
    > 
    > To verify you have the updated version of the tool, run ``buildthemes --version`` again.

20. Ensure your local 'working' ``sources`` directory is up to date (since this is required for updated versions of the WebApp Theme Builder tool to function correctly). To do this:

    10. Identify the directory where the WebApp Theme Builder tool was installed by the ``npm`` command. This directory should be named ``covata-theme-builder``.

        > **Note:** On Mac OS X / Linux-based computers, this directory is typically within ``/usr/local/lib/node_modules``. On Microsoft Windows-based computers, this directory is the location of the ``node.exe`` binary.

    20. Change directory into the ``sources/system`` subdirectory of ``covata-theme-builder``.

    30. Ensure that the types of image/logo files and properties in the ``theme.properties`` file in each subdirectory of your local 'working' ``sources`` directory are consistent with those in this ``sources/system`` subdirectory of ``covata-theme-builder`` (which will contain any updates from the previous ``npm update`` step).

        > **Tip:** It might be easier opening your local 'working' ``sources`` directory in a separate window to perform this substep.

        * For any new image/logo files *and* properties of the ``theme.properties`` file in the ``sources/system`` subdirectory that do not exist in the subdirectories of your 'working' ``sources`` directory, copy these across to each of these subdirectories.

        * ( *Optional, but recommended for tidiness* ) Similarly, for any image/logo files *and* properties of the ``theme.properties`` file in the subdirectories of your 'working' ``sources`` directory that do not exist in the ``sources/system`` subdirectory, remove the relevant files/properties in ``theme.properties`` from the subdirectories of your 'working' ``sources`` directory.


## Additional ``buildthemes`` command options ##

The ``buildthemes`` command has some additional options.

For help on what you can do with the tool, type the command:

``buildthemes --help``

### Command option details ###

10. ``-o, --output`` - allows you to specify a different location and name to which the content of the ``output`` directory will be generated.

    * Example for Mac OS X or Linux: ``buildthemes -o /User/someuser/safesharethemes/output``

    * Example for Windows: ``buildthemes -o C:\Users\someuser\safesharethemes\output``

    * Simply specify the directory name (eg. ``buildthemes -o output``) to generate the ``output`` directory within your current directory.

20. ``-s, --source`` - allows you to specify a different location and name to which the content of the ``sources`` directory will be read from.

    * Example on Mac or Linux: ``buildthemes -s /User/someuser/safesharethemes/sources``.

    * Example on Windows: ``buildthemes -s C:\Users\someuser\ safesharethemes\sources``.

    * Simply specify the directory name (eg. ``buildthemes -o sources``) if the ``sources`` directory is located within your current directory.

30. ``-c, --config`` - allows you to specify a different location and name for the ``settings.json`` file. This file 'pre-defines' the location and name of your ``sources`` and ``output`` directories.

    * Example for Mac OS X or Linux: ``buildthemes -c /User/someuser/safesharethemes/settings.json``

    * Example for Windows: ``buildthemes -c C:\Users\someuser\safesharethemes\settings.json``

    * Simply specify the name of this file (eg. ``buildthemes -c settings.json``) if this file is located within your current directory.


## Changelog ##

**Changes in 0.1.12 - 0.1.14**

* Introduced a new field to customise the Create Button Color, and ``README.md`` file updates.

**Changes in 0.1.11**

* Introduced a new footer-logo-white logo/image that appears after user login, and ``README.md`` file updates.

**Changes in 0.1.10**

* ``README.md`` file updates - Content/detail improvements and corrections.

**Changes in 0.1.9**

* ``buildthemes`` now allows the colors of success messages (which appear temporarily over the header) to be customised.

**Changes in 0.1.8**

* Fixed a bug in which the **SIGN IN** button was determined by the ``primary-button-color`` property instead of ``primary-color``. ``buildthemes`` now correctly uses the ``primary-color`` property value for its color.

**Changes in 0.1.3 - 0.1.7**

* ``README.md`` file updates - Minor formatting fixes + content/detail improvements and corrections.

**Changes in 0.1.2**

* ``README.md`` file updates - Added new sections - one on 'Upgrading the WebApp Theme Builder tool' and another for this Changelog.

* Fixed the functionality of the ``builthemes`` command's ``--version`` option.

**Changes in 0.1.1**

* ``README.md`` file updates - Documented the footer customisation changes in 0.1.0.

**Changes in 0.1.0**

* ``buildthemes`` now allows the footer's background and text colors to be customised.
